# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'run_cl/version'

Gem::Specification.new do |gem|
  gem.name          = "run_cl"
  gem.version       = RunCl::VERSION
  gem.authors       = ["mespina"]
  gem.email         = ["mespina.icc@gmail.com"]
  gem.summary       = "Rut Chilenos"
  gem.description   = "Formateador/Desformateador, Generador, Validador de Rut Chilenos"
  gem.homepage      = "https://bitbucket.org/mespina/run"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
