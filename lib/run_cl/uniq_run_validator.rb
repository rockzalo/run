# encoding: utf-8

class UniqRunValidator < ActiveModel::Validator
  def validate record
    options[:attributes].each do |attribute|
      run = Run.remove_format record.send(attribute)

      matchers = []
      if record.new_record?
        matchers = record.class.where(attribute => run)
      else
        matchers = record.class.where('? = ? AND id != ?', attribute, run, record.id)
      end

      if options[:scope]
        matchers = matchers.where(options[:scope] => record.send(options[:scope]))
      end

      matchers.any?

      record.errors[attribute.to_sym] << 'ya está en uso!!!' if matchers.any?
    end
  end

  private
  # TODO - locales
  # record.errors[attribute.to_sym] << I18n.t('run.in_use') unless Run.valid? record.send(attribute)
end